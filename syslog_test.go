package syslog_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/miatel/go/syslog"
)

func TestSyslog(t *testing.T) {
	sl, err := syslog.New(
		syslog.WithPriority(syslog.LOG_WARNING|syslog.LOG_LOCAL0),
		syslog.WithTag("test"),
	)
	require.NoError(t, err)

	err = sl.Write(syslog.LOG_WARNING, []byte("test"))
	require.NoError(t, err)
}

func TestSyslogLong(t *testing.T) {
	sl, err := syslog.New(
		syslog.WithPriority(syslog.LOG_WARNING|syslog.LOG_LOCAL0),
		syslog.WithTag("test"),
	)
	require.NoError(t, err)

	bytes := make([]byte, 0, 10000)
	for len(bytes) < 10000 {
		bytes = append(bytes, []byte("test")...)
	}

	err = sl.Write(syslog.LOG_WARNING, bytes)
	require.NoError(t, err)
}

func TestSyslogWithNewLine(t *testing.T) {
	sl, err := syslog.New(
		syslog.WithPriority(syslog.LOG_WARNING|syslog.LOG_LOCAL0),
		syslog.WithTag("test"),
	)
	require.NoError(t, err)

	err = sl.Write(syslog.LOG_WARNING, []byte("test\n"))
	require.NoError(t, err)
}

func BenchmarkSyslog(b *testing.B) {
	sl, err := syslog.New(
		syslog.WithPriority(syslog.LOG_WARNING|syslog.LOG_LOCAL0),
		syslog.WithTag("test"),
	)
	require.NoError(b, err)

	b.ReportAllocs()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		err = sl.Write(syslog.LOG_WARNING, []byte("test"))
		require.NoError(b, err)
	}
}
