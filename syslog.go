package syslog

import (
	"errors"
	"fmt"
	"net"
	"os"
	"path"
	"strconv"
	"sync"
	"syscall"
	"time"
)

// The Priority is a combination of the syslog facility and
// severity. For example, LOG_ALERT | LOG_FTP sends an alert severity
// message from the FTP facility. The default severity is LOG_EMERG;
// the default facility is LOG_USER.
type Priority int

const severityMask = 0x07
const facilityMask = 0xf8

const (
	// Severity

	// From /usr/include/sys/syslog.h.
	// These are the same on Linux, BSD, and OS X.

	LOG_EMERG Priority = iota
	LOG_ALERT
	LOG_CRIT
	LOG_ERR
	LOG_WARNING
	LOG_NOTICE
	LOG_INFO
	LOG_DEBUG
)

const (
	// Facility

	// From /usr/include/sys/syslog.h.
	// These are the same up to LOG_FTP on Linux, BSD, and OS X.

	LOG_KERN Priority = iota << 3
	LOG_USER
	LOG_MAIL
	LOG_DAEMON
	LOG_AUTH
	LOG_SYSLOG
	LOG_LPR
	LOG_NEWS
	LOG_UUCP
	LOG_CRON
	LOG_AUTHPRIV
	LOG_FTP
	_ // unused
	_ // unused
	_ // unused
	_ // unused
	LOG_LOCAL0
	LOG_LOCAL1
	LOG_LOCAL2
	LOG_LOCAL3
	LOG_LOCAL4
	LOG_LOCAL5
	LOG_LOCAL6
	LOG_LOCAL7
)

const maxLine = 8192

type Writer struct {
	priority Priority
	hostname string
	tag      string
	pid      int64

	network string
	raddr   string

	buf  []byte
	conn net.Conn

	mu sync.Mutex
}

func New(options ...Option) (*Writer, error) {
	cfg := config{}
	for _, opt := range options {
		opt(&cfg)
	}

	if (cfg.priority & ^(facilityMask | severityMask)) > 0 {
		return nil, fmt.Errorf("syslog: unknown facility/priority: %x", cfg.priority)
	}

	if (cfg.priority & facilityMask) == 0 {
		// This is the default facility identifier if none is specified.
		cfg.priority |= LOG_USER
	}

	cfg.hostname, _ = os.Hostname()

	if cfg.tag == "" {
		cfg.tag = path.Base(os.Args[0])
	}

	v := &Writer{
		priority: cfg.priority,
		hostname: cfg.hostname,
		tag:      cfg.tag,
		pid:      int64(os.Getpid()),
		network:  cfg.network,
		raddr:    cfg.raddr,
		buf:      make([]byte, 0, maxLine),
	}

	v.mu.Lock()
	defer v.mu.Unlock()

	if err := v.connect(); err != nil {
		return nil, err
	}

	return v, nil
}

func (v *Writer) connect() error {
	if v.conn != nil {
		v.conn.Close()
		v.conn = nil
	}

	var (
		err error
		c   net.Conn
	)

	if v.network == "" {
		if c, err = unixConnect(); err != nil {
			return err
		}

		if v.hostname == "" {
			v.hostname = "localhost"
		}
	} else {
		if c, err = net.Dial(v.network, v.raddr); err != nil {
			return err
		}

		if v.hostname == "" {
			v.hostname = c.LocalAddr().String()
		}
	}

	v.conn = c

	return nil
}

func (v *Writer) Close() error {
	v.mu.Lock()
	defer v.mu.Unlock()

	if v.conn != nil {
		err := v.conn.Close()
		v.conn = nil
		return err
	}

	return nil
}

func (v *Writer) Write(p Priority, b []byte) error {
	return v.writeAndRetry(p, b)
}

func (v *Writer) writeAndRetry(p Priority, b []byte) error {
	pr := (v.priority & facilityMask) | (p & severityMask)

	v.mu.Lock()
	defer v.mu.Unlock()

	if v.conn != nil {
		if err := v.write(pr, b); err == nil {
			return nil
		}
	}

	if err := v.connect(); err != nil {
		return err
	}

	return v.write(pr, b)
}

const rfc3339Micro = "2006-01-02T15:04:05.999999Z07:00"

func (v *Writer) write(p Priority, msg []byte) error {
	v.buf = v.buf[:0]

	// Priority
	v.buf = append(v.buf, '<')
	v.buf = strconv.AppendInt(v.buf, int64(p), 10)
	v.buf = append(v.buf, ">1 "...)

	// Timestamp
	v.buf = time.Now().AppendFormat(v.buf, rfc3339Micro)
	v.buf = append(v.buf, ' ')

	// Hostname
	v.buf = append(v.buf, v.hostname...)
	v.buf = append(v.buf, ' ')

	// Tag
	v.buf = append(v.buf, v.tag...)
	v.buf = append(v.buf, ' ')

	// PID
	v.buf = strconv.AppendInt(v.buf, v.pid, 10)

	// Message ID, Structured data
	v.buf = append(v.buf, " - - "...)

	// Message
	if len(msg) >= 1 && msg[len(msg)-1] == '\n' {
		msg = msg[:len(msg)-1]
	}
	v.buf = append(v.buf, msg...)

	_, err := v.conn.Write(v.buf)
	if err != nil {
		if len(v.buf) > maxLine && errors.Is(err, syscall.EMSGSIZE) {
			_, err = v.conn.Write(v.buf[:maxLine])
		}

		return err
	}

	return nil
}
